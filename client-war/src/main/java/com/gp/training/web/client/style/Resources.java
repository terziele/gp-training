package com.gp.training.web.client.style;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;

public interface Resources extends ClientBundle{	
	
	public static Resources r = GWT.create(Resources.class);

	public interface Styles extends CssResource {		
		
		public String inlineBlock();
		public String mainTable();
		public String border1();
		public String padding5();
		public String marginTopBottom5();
		public String floatRight();
		public String mainContent();
		public String marginLeft();
		public String Button();
		public String callBox();
		public String cell();
		public String table();
	}
	
	
	@Source({"css/Style.gss"})
	public Styles styles();
}
