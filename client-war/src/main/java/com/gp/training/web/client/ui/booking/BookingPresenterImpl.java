package com.gp.training.web.client.ui.booking;

import com.google.gwt.core.client.Callback;
import com.gp.training.web.shared.model.BookingProxy;
import com.gp.training.web.shared.service.BookingResource;
import org.jboss.errai.common.client.api.RemoteCallback;
import org.jboss.errai.enterprise.client.jaxrs.api.RestClient;

/**
 * Created by tondi on 2/5/16.
 */
public class BookingPresenterImpl implements BookingPresenter {
	@Override
	public void getBooking(int id, final Callback<BookingProxy, Void> bookingCallback) {
		RestClient.create(BookingResource.class, new RemoteCallback<BookingProxy>() {
			@Override
			public void callback(BookingProxy response) {
				if(response != null) {
					bookingCallback.onSuccess(response);
				}
			}
		}).get((long)id);
	}
}
