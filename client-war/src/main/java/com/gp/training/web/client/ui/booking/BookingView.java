package com.gp.training.web.client.ui.booking;

import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.gp.training.web.client.params.PageParams;
import com.gp.training.web.client.ui.common.GenericBaseView;
import com.gp.training.web.client.ui.common.PageBaseView;
import com.gp.training.web.client.ui.customers.customer.CustomerPresenter;
import com.gp.training.web.shared.model.BookingProxy;
import com.gp.training.web.shared.model.CustomerProxy;
import com.gp.training.web.shared.model.HotelProxy;
import com.sksamuel.jqm4gwt.JQMContext;
import com.sksamuel.jqm4gwt.button.JQMButton;
import com.sksamuel.jqm4gwt.form.elements.JQMNumber;
import com.sksamuel.jqm4gwt.form.elements.JQMText;
import com.sksamuel.jqm4gwt.html.Div;
import com.sksamuel.jqm4gwt.html.Span;
import com.sksamuel.jqm4gwt.layout.JQMTable;
import com.sksamuel.jqm4gwt.list.JQMList;
import com.sksamuel.jqm4gwt.list.JQMListItem;

import java.util.List;

public class BookingView extends GenericBaseView<BookingPresenter> implements PageBaseView {
	private PageParams pageParams;
	private Callback<BookingProxy, Void> bookingCallback;
	private Callback<List<CustomerProxy>, Void> customerCallback;

	private BookingProxy booking;
	@UiField
	protected Div countryText;
	@UiField
	protected Div cityText;
	@UiField
	protected Div hotelText;
	@UiField
	protected Div roomTypeText;
	@UiField
	protected Div mealTypeText;
	@UiField
	protected Div datesSpan;
	@UiField
	protected JQMNumber bookingId;
	@UiField
	protected JQMButton sendBtn;
	@UiField
	protected JQMTable customersTable;


	private static BookingViewUiBinder uiBinder = GWT.create(BookingViewUiBinder.class);

	@Override
	public void loadPageParams(PageParams params) {
		pageParams = params != null ? params : new PageParams();
		bookingId.setValue(String.valueOf(pageParams.getBookingId()));
		showBookingInfo();
	}


	private void showBookingInfo() {
		setHash(Integer.parseInt(bookingId.getValue()));
		int id = getIdFromHash();
		pageParams.setBookingId(id);

		getPresenter().getBooking(pageParams.getBookingId(), bookingCallback);
	}

	interface BookingViewUiBinder extends UiBinder<Widget, BookingView> {
	}

	public BookingView() {
		initWidget(uiBinder.createAndBindUi(this));
		setPresenter(new BookingPresenterImpl());
		initCallbacks();

	}

	private void initCallbacks() {

		bookingCallback = new Callback<BookingProxy, Void>() {
			@Override
			public void onFailure(Void aVoid) {

			}

			@Override
			public void onSuccess(BookingProxy bookingProxy) {
				booking = bookingProxy;

				HotelProxy hotel = booking.getOffer().getHotel();
				countryText.setHTML("Country: " + hotel.getCity().getCountry().getName());
				cityText.setText("City: " + hotel.getCity().getName());
				hotelText.setText("Hotel: " + hotel.getName());

				mealTypeText.setText("Meal type: " + booking.getMealType().getName());

				datesSpan.setText("Dates from " +
						booking.getStartDate().toString() +
						" till " + booking.getEndDate().toString());
				roomTypeText.setText("Room type: " + booking.getOffer().getRoom().getName());
				customersTable.clear();
				for (CustomerProxy customer : bookingProxy.getCustomers()) {

					Label id = new Label();
					id.setText(customer.getId() + "");
					Label name = new Label();
					name.setText(customer.getFirstName() + " " + customer.getLastName());
					customersTable.add(id);
					customersTable.add(name);
				}
			}
		};
	}

	private void setHash(int id) {
		Window.Location.assign(Window.Location.createUrlBuilder()
				.setHash(id + "").buildString());

	}

	private int getIdFromHash() {
		return Integer.valueOf(Window.Location.getHash().substring(1));
	}

	@UiHandler("sendBtn")
	void onClick(ClickEvent e) {
		showBookingInfo();
	}

}
