package com.gp.training.web.client.ui.common;

/**
 * Created by tondi on 2/6/16.
 */
public class FieldValidator {
	private static final String NAME_PATTERN = "[a-zA-Z]{2,20}";
	private static final String EMAIL_PATTERN = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
	private static final String PHONE_PATTERN = "\\+?[0-9]{12}";
	private FieldValidator() {}
	public static boolean isNameValid(String value) {

		return value != null ? value.matches(NAME_PATTERN) : false;
	}

	public static boolean isEmailValid(String value) {
		return value != null ? value.matches(EMAIL_PATTERN) : false;
	}

	public static boolean isPhoneValid(String value) {
		return value != null ? value.matches(PHONE_PATTERN) : false;
	}
}
