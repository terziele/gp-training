package com.gp.training.web.client.ui.customers;

import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import com.gp.training.web.client.app.AppContext;
import com.gp.training.web.client.app.PageToken;
import com.gp.training.web.client.params.PageParams;
import com.gp.training.web.client.ui.common.GenericBaseView;
import com.gp.training.web.client.ui.common.PageBaseView;
import com.gp.training.web.client.ui.customers.customer.CustomerItem;
import com.gp.training.web.client.ui.customers.customer.CustomerPresenter;
import com.gp.training.web.client.ui.customers.customer.CustomerPresenterImpl;
import com.gp.training.web.shared.model.*;
import com.sksamuel.jqm4gwt.button.JQMButton;
import com.sksamuel.jqm4gwt.form.elements.JQMSelect;
import com.sksamuel.jqm4gwt.list.JQMList;
import com.sksamuel.jqm4gwt.list.JQMListItem;

import java.util.ArrayList;
import java.util.List;

public class CustomersView extends GenericBaseView<CustomerPresenter> implements PageBaseView {

	private PageParams pageParams;
	private Callback<BookingProxy, Void> bookingCallback;

	@UiField
	protected JQMButton nextBtn;
	@UiField
	protected JQMList guestList;
	@UiField
	protected JQMSelect mealTypeSelect;

	private static CustomersViewUiBinder uiBinder = GWT.create(CustomersViewUiBinder.class);

	interface CustomersViewUiBinder extends UiBinder<Widget, CustomersView> {
	}

	public CustomersView() {
		initWidget(uiBinder.createAndBindUi(this));
		setPresenter(new CustomerPresenterImpl());
		initCallbacks();
		getMealTypes();
	}

	private void getMealTypes() {
		getPresenter().getMealTypes(new Callback<List<MealTypeProxy>, Void>() {
			@Override
			public void onFailure(Void aVoid) {

			}

			@Override
			public void onSuccess(List<MealTypeProxy> response) {
				mealTypeSelect.clear();
				for (MealTypeProxy proxy : response) {
					mealTypeSelect.addOption(String.valueOf(proxy.getId()), proxy.getName());
				}
				mealTypeSelect.refresh();
			}
		});
	}

	private void initCallbacks() {
		bookingCallback = new Callback<BookingProxy, Void>() {
			@Override
			public void onFailure(Void aVoid) {

			}

			@Override
			public void onSuccess(BookingProxy bookingProxy) {
				//todo put booking to all the customers and then send them on bizlogic
				List<CustomerProxy> customers = new ArrayList<>();
				List<JQMListItem> items = guestList.getItems();
				for (JQMListItem item : items) {
					int count = item.getWidgetCount();
					for (int i = 0; i < count; i++) {
						Widget w = item.getWidget(i);
						if (w instanceof CustomerItem) {
							CustomerItem ci = (CustomerItem) w;
							CustomerProxy customer = new CustomerProxy();
							customer.setFirstName(ci.firstNameText.getValue());
							customer.setLastName(ci.lastNameText.getValue());
							customer.setEmail(ci.email.getValue());

							customer.setPhone(ci.phoneText.getValue());
							customer.setBookingId(bookingProxy.getId());
							customers.add(customer);
						}

					}
				}
				getPresenter().createCustomers(customers);

				pageParams.setBookingId(bookingProxy.getId());
				AppContext.navigationService.next(PageToken.BOOKING, pageParams);
			}
		};
	}


	@Override
	public void loadPageParams(PageParams params) {
		pageParams = params != null ? params : new PageParams();
		recreateGuestsList();
	}

	private void recreateGuestsList() {
		guestList.clear();

		int guestCount = pageParams.getGuestCount();

		for (int i = 0; i < guestCount; i++) {
			JQMListItem listItem = new JQMListItem();
			listItem.setControlGroup(true, false);

			CustomerItem customer = new CustomerItem();
			listItem.addWidget(customer);
			guestList.appendItem(listItem);
		}

		guestList.recreate();
		guestList.refresh();
	}

	@UiHandler("nextBtn")
	void onNextClick(ClickEvent e) {
		if(areFieldsValid()) {
			BookingCreateProxy proxy = new BookingCreateProxy();
			proxy.setOfferId(pageParams.getOfferId());
			proxy.setMealTypeId(Integer.parseInt(mealTypeSelect.getSelectedValue()));
			proxy.setStartDate(pageParams.getStartDate());
			proxy.setEndDate(pageParams.getEndDate());
			proxy.setGuestCount(pageParams.getGuestCount());

			getPresenter().create(proxy, bookingCallback);
		} else {
			Window.alert("Some of your fields are not filled properly. Please check.");
		}

	}

	private boolean areFieldsValid() {
		List<JQMListItem> items = guestList.getItems();
		for (JQMListItem item : items) {
			int count = item.getWidgetCount();
			for (int i = 0; i < count; i++) {
				Widget w = item.getWidget(i);
				if (w instanceof CustomerItem) {
					CustomerItem ci = (CustomerItem) w;
					if(!ci.isItemValid()) {
						return false;
					}
				}

			}
		}
		return true;
	}
}
