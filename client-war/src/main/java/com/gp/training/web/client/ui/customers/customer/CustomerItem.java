package com.gp.training.web.client.ui.customers.customer;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.gp.training.web.client.ui.common.FieldValidator;
import com.sksamuel.jqm4gwt.form.elements.JQMEmail;
import com.sksamuel.jqm4gwt.form.elements.JQMText;

public class CustomerItem extends Composite {
	private boolean isFirstNameValid;
	private boolean isLastNameValid;
	private boolean isEmailValid;
	private boolean isPhoneValid;

	@UiField
	public JQMText firstNameText;
	@UiField
	public JQMText lastNameText;
	@UiField
	public JQMEmail email;
	@UiField
	public JQMText phoneText;
	@UiField
	public Label errorLabel;

	private static CustomerItemUiBinder uiBinder = GWT.create(CustomerItemUiBinder.class);

	interface CustomerItemUiBinder extends UiBinder<Widget, CustomerItem> {
	}

	public CustomerItem() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@UiHandler("firstNameText")
	void onFirstNameValueChanged(ValueChangeEvent<String> e) {
		String value = firstNameText.getValue();
		isFirstNameValid = FieldValidator.isNameValid(value);
		if(isFirstNameValid) {
			errorLabel.setVisible(false);
		} else {
			errorLabel.setText("First name is not valid.");
			errorLabel.setVisible(true);
		}
	}
	@UiHandler("lastNameText")
	void onLastNameValueChanged(ValueChangeEvent<String> e) {
		String value = lastNameText.getValue();
		isLastNameValid = FieldValidator.isNameValid(value);
		if(isLastNameValid) {
			errorLabel.setVisible(false);
		} else {
			errorLabel.setText("Last name is not valid.");
			errorLabel.setVisible(true);
		}
	}

	@UiHandler("email")
	void onEmailValueChange(ValueChangeEvent<String> e) {
		String value = email.getValue();
		isEmailValid = FieldValidator.isEmailValid(value);
		if(isEmailValid) {
			errorLabel.setVisible(false);
		} else {
			errorLabel.setText("E-mail is not valid.");
			errorLabel.setVisible(true);
		}
	}

	@UiHandler("phoneText")
	void onPhoneValueChange(ValueChangeEvent<String> e) {
		String value = phoneText.getValue();
		isPhoneValid = FieldValidator.isPhoneValid(value);
		if(isPhoneValid) {
			errorLabel.setVisible(false);
		} else {
			errorLabel.setText("Phone number is not valid.");
			errorLabel.setVisible(true);
		}
	}

	public boolean isItemValid() {
		return isFirstNameValid &&
				isLastNameValid &&
				isEmailValid &&
				isPhoneValid;
	}
}
