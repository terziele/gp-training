package com.gp.training.web.client.ui.customers.customer;

import com.google.gwt.core.client.Callback;
import com.gp.training.web.client.ui.common.Presenter;
import com.gp.training.web.shared.model.BookingCreateProxy;
import com.gp.training.web.shared.model.BookingProxy;
import com.gp.training.web.shared.model.CustomerProxy;
import com.gp.training.web.shared.model.MealTypeProxy;

import java.util.List;


public interface CustomerPresenter extends Presenter {

	public BookingProxy get(long id);
//	public BookingProxy create(BookingProxy proxy);
//	public BookingProxy create(Booking)
	public void getMealTypes(Callback<List<MealTypeProxy>, Void> callback);

	void create(BookingProxy booking, Callback<BookingProxy, Void> bookingCallback);
	void create(BookingCreateProxy booking, Callback<BookingProxy, Void> bookingCallback);

	void createCustomers(List<CustomerProxy> customers);
}
