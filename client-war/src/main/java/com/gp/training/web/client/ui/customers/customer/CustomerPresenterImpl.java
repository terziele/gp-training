package com.gp.training.web.client.ui.customers.customer;

import com.google.gwt.core.client.Callback;
import com.gp.training.web.client.common.DictionaryPresenter;
import com.gp.training.web.shared.model.BookingCreateProxy;
import com.gp.training.web.shared.model.BookingProxy;
import com.gp.training.web.shared.model.CustomerProxy;
import com.gp.training.web.shared.model.MealTypeProxy;
import com.gp.training.web.shared.service.BookingResource;
import com.gp.training.web.shared.service.CustomerResource;
import org.jboss.errai.common.client.api.RemoteCallback;
import org.jboss.errai.enterprise.client.jaxrs.api.RestClient;

import java.util.List;


public class CustomerPresenterImpl implements CustomerPresenter  {



	private DictionaryPresenter dictionaryPresenter;

	public CustomerPresenterImpl() {
		this.dictionaryPresenter = new DictionaryPresenter();
	}

	@Override
	public BookingProxy get(long id) {
		return null;
	}


	@Override
	public void getMealTypes(Callback<List<MealTypeProxy>, Void> callback) {
		dictionaryPresenter.getMealTypes(callback);
	}

	@Override
	public void create(BookingProxy booking, final Callback<BookingProxy, Void> originCallback) {
		RestClient.create(BookingResource.class, new RemoteCallback<BookingProxy>() {
			@Override
			public void callback(BookingProxy bookingProxy) {
				if(originCallback != null && bookingProxy != null) {
					originCallback.onSuccess(bookingProxy);
				}
			}
		})/*.create(booking)*/;
	}

	@Override
	public void create(BookingCreateProxy booking, final Callback<BookingProxy, Void> originCallback) {
		RestClient.create(BookingResource.class, new RemoteCallback<BookingProxy>() {
			@Override
			public void callback(BookingProxy bookingProxy) {
				if(originCallback != null && bookingProxy != null) {
					originCallback.onSuccess(bookingProxy);
				}
			}
		}).create(booking);
	}

	@Override
	public void createCustomers(List<CustomerProxy> customers) {
		RestClient.create(CustomerResource.class, new RemoteCallback<List<CustomerProxy>>() {
			@Override
			public void callback(List<CustomerProxy> customerProxies) {

			}
		}).create(customers);
	}

}
