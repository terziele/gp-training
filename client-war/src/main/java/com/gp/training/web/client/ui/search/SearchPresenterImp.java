package com.gp.training.web.client.ui.search;

import java.util.List;

import org.jboss.errai.common.client.api.RemoteCallback;
import org.jboss.errai.enterprise.client.jaxrs.api.RestClient;

import com.google.gwt.core.client.Callback;
import com.gp.training.web.client.common.DictionaryPresenter;
import com.gp.training.web.shared.model.CityProxy;
import com.gp.training.web.shared.model.CountryProxy;
import com.gp.training.web.shared.model.OfferProxy;
import com.gp.training.web.shared.params.OfferSearchParams;
import com.gp.training.web.shared.service.AvailabilityResource;

public class SearchPresenterImp implements SearchPresenter {
	
	private DictionaryPresenter dictionaryPresenter;
	
	public SearchPresenterImp() {
		dictionaryPresenter = new DictionaryPresenter();
				
	}

	@Override
	public void loadCountries(Callback<List<CountryProxy>, Void> callback) {
		dictionaryPresenter.getCountries(callback);
		
	}

	@Override
	public void loadCities(Callback<List<CityProxy>, Void> callback) {
		dictionaryPresenter.getCities(callback);
	}

	@Override
	public void getOffers(OfferSearchParams params, final Callback<List<OfferProxy>, Void> callback) {
		RestClient.create(AvailabilityResource.class, new RemoteCallback<List<OfferProxy>>() {
			@Override
			public void callback(List<OfferProxy> response) {			
				if (callback != null) callback.onSuccess(response);
			}
		}).getOffers(params.getCityId(), params.getGuestCount(), params.getStartDate(), params.getEndDate());
		
	}

	@Override
	public void getCitiesByCountry(Long id, Callback<List<CityProxy>, Void> callback) {
		dictionaryPresenter.getCitiesByCountry(id, callback);
	}
}
