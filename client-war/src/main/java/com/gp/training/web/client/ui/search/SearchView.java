package com.gp.training.web.client.ui.search;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Widget;
import com.gp.training.web.client.app.AppContext;
import com.gp.training.web.client.app.PageToken;
import com.gp.training.web.client.params.PageParams;
import com.gp.training.web.client.ui.common.GenericBaseView;
import com.gp.training.web.client.ui.common.PageBaseView;
import com.gp.training.web.client.ui.search.offer.OfferDescItem;
import com.gp.training.web.shared.model.CityProxy;
import com.gp.training.web.shared.model.CountryProxy;
import com.gp.training.web.shared.model.OfferProxy;
import com.gp.training.web.shared.params.OfferSearchParams;
import com.sksamuel.jqm4gwt.JQMContext;
import com.sksamuel.jqm4gwt.button.JQMButton;
import com.sksamuel.jqm4gwt.form.elements.JQMSelect;
import com.sksamuel.jqm4gwt.html.Div;
import com.sksamuel.jqm4gwt.list.JQMList;
import com.sksamuel.jqm4gwt.list.JQMListDivider;
import com.sksamuel.jqm4gwt.list.JQMListItem;
import com.sksamuel.jqm4gwt.plugins.datebox.JQMCalBox;

public class SearchView extends GenericBaseView<SearchPresenter> implements PageBaseView {
	
	private static final int MAX_GUEST_COUNT = 4;
	
	private static SearchViewUiBinder uiBinder = GWT.create(SearchViewUiBinder.class);
	
	private Callback<List<OfferProxy>, Void> offerSearchCallback;
	
	private OfferProxy selectedOffer;
	private List<OfferDescItem> offerDescItems = new ArrayList<OfferDescItem>();
	private Callback<OfferProxy, Void> offerSelectCallback;
	private Callback<List<CityProxy>, Void> cityListCallback;

	@UiField
	protected JQMButton searchBtn;
	@UiField
	protected JQMSelect guestCountSelect;
	@UiField
	protected JQMSelect countrySelect;
	@UiField
	protected JQMSelect citySelect;
	@UiField
	protected JQMCalBox startDate;
	@UiField
	protected JQMCalBox endDate;
	@UiField
	protected JQMList offersList;
	@UiField
	protected Div noOffersText;
	@UiField
	protected JQMButton nextBtn;
	
	interface SearchViewUiBinder extends UiBinder<Widget, SearchView> {
	}

	public SearchView() {
		initWidget(uiBinder.createAndBindUi(this));
		
		setPresenter(new SearchPresenterImp());
		
		initControls();
		buildOffersCallback();
		buildOfferSelectCallback();

	}
	
	private void initControls() {
		initGuestCountSelect();
		loadDictionaries();
		startDate.withClearButton(false);

	}
	
	private void initGuestCountSelect() {
		for (int i=1; i<=MAX_GUEST_COUNT; i++ ) {
			guestCountSelect.addOption(String.valueOf(i));			
		}
		
		guestCountSelect.refresh();
	}
	
	private void loadDictionaries() {
		cityListCallback = new Callback<List<CityProxy>, Void>() {
			@Override
			public void onFailure(Void reason) {

			}

			@Override
			public void onSuccess(List<CityProxy> result) {
				if (result != null && result.size() > 0) {
					citySelect.clear();
					for (CityProxy proxy : result) {
						citySelect.addOption(String.valueOf(proxy.getId()), proxy.getName());
					}
					citySelect.refresh();
				}
			}
		};
		getPresenter().loadCountries(new Callback<List<CountryProxy>, Void>() {
			@Override
			public void onSuccess(List<CountryProxy> result) {
				if (result != null && result.size() > 0) {
					for (CountryProxy proxy : result) {						
						countrySelect.addOption(String.valueOf(proxy.getId()), proxy.getName());
					}
					countrySelect.refresh();

					getPresenter().getCitiesByCountry(Long.valueOf(countrySelect.getSelectedValue()), cityListCallback);
				}
			}		
			@Override
			public void onFailure(Void reason) {
				//do nothing
			}
		});
		
//		getPresenter().loadCities(cityListCallback);
	}

	
	private PageParams buildPageParams() {
		PageParams params = new PageParams();

		String guestCountStr = guestCountSelect.getValue();
		params.setGuestCount(Integer.valueOf(guestCountStr));
		params.setOfferId(selectedOffer.getOfferId());
		params.setStartDate(startDate.getDate());
		params.setEndDate(endDate.getDate());
		
		
		/*params.setStartDate(startDateStr);
		params.setEndDate(endDateStr);*/
		
		return params;
	}
	
	private void buildOffersCallback() {
		offerSearchCallback = new Callback<List<OfferProxy>, Void>() {
			
			@Override
			public void onSuccess(List<OfferProxy> result) {
				offersList.clear();
				offerDescItems.clear();
				if (result == null || result.size() == 0) {
					offersList.setVisible(false);
					noOffersText.setVisible(true);
				} else {
					noOffersText.setVisible(false);
					populateOffersList(result);
				}				
			}

			@Override
			public void onFailure(Void reason) {				
			}
		};
	}
	
	private void populateOffersList(List<OfferProxy> offers) {
		offersList.clear();
		
		for (OfferProxy offer : offers) {
			JQMListItem item = new JQMListItem();
			item.setControlGroup(true, false);
			
			final OfferDescItem descItem = new OfferDescItem();
			descItem.populate(offer);
			item.addWidget(descItem);
			
			descItem.selectOfferCheckBox.addValueChangeHandler(new ValueChangeHandler<Boolean>() {			
				@Override
				public void onValueChange(ValueChangeEvent<Boolean> event) {
					if (event.getValue()) {
						offerSelectCallback.onSuccess(descItem.getOfferProxy());
					} else {
						selectedOffer = null;
					}
				};
			});
			
			offersList.appendItem(item);
//			offersList.appendDivider(new JQMListDivider());
			
			offerDescItems.add(descItem);
		}
		
		offersList.refresh();
        JQMContext.render(offersList.getElement());
        
        offersList.setVisible(true);
	}
	
	private void buildOfferSelectCallback() {
		offerSelectCallback = new Callback<OfferProxy, Void>() {
			
			@Override
			public void onSuccess(OfferProxy result) {
				int offerId = result.getOfferId();
				
				for (OfferDescItem descItem : offerDescItems) {
					OfferProxy offer = descItem.getOfferProxy();
					descItem.selectOfferCheckBox.setValue(offer.getOfferId() == offerId, false);
				}
				selectedOffer = result;
			}
			
			@Override
			public void onFailure(Void reason) {
				// TODO Auto-generated method stub
				
			}
		};


	}

	@Override
	public void loadPageParams(PageParams params) {
		
	}

	@UiHandler("countrySelect")
	void countrySelectOnValueChange(ValueChangeEvent<String> e) {
		getPresenter().getCitiesByCountry(Long.parseLong(countrySelect.getSelectedValue()), cityListCallback);
	}

	@UiHandler("searchBtn")
	void searchBtnOnClick(ClickEvent e) {
		String cityIdStr = citySelect.getValue();
		String guestCountStr = guestCountSelect.getValue();
		String startDateStr = startDate.getValue();
		String endDateStr = endDate.getValue();

		OfferSearchParams params = new OfferSearchParams();
		params.setCityId(Integer.valueOf(cityIdStr));
		params.setGuestCount(Integer.valueOf(guestCountStr));
		params.setStartDate(startDateStr);
		params.setEndDate(endDateStr);

		getPresenter().getOffers(params, offerSearchCallback);
	}


	@UiHandler("nextBtn")
	void onNextClick(ClickEvent e) {
		if (selectedOffer != null) {
			PageParams params = buildPageParams();
			AppContext.navigationService.next(PageToken.CUSTOMERS, params);
		}
	}

}
