package com.gp.training.web.server.service;

import com.gp.training.bizlogic.api.model.CustomerDTO;
import com.gp.training.bizlogic.api.params.CustomerCreateParams;
import com.gp.training.bizlogic.client.resource.CustomerService;
import com.gp.training.web.server.utils.ProxyConverter;
import com.gp.training.web.shared.model.CustomerProxy;
import com.gp.training.web.shared.service.CustomerResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerResourceImpl implements CustomerResource {

	@Autowired
	private CustomerService customerService;

	@Override
	public List<CustomerProxy> create(List<CustomerProxy> customers) {
		List<CustomerCreateParams> params = new ArrayList<>(customers.size());
		for (CustomerProxy proxy : customers) {
			CustomerCreateParams param = new CustomerCreateParams.Builder()
					.bookingId(proxy.getBookingId())
					.email(proxy.getEmail())
					.firstName(proxy.getFirstName())
					.lastName(proxy.getLastName())
					.phone(proxy.getPhone())
					.build();
			params.add(param);
		}
		List<CustomerDTO> dtos = customerService.create(params);
		customers.clear();
		for(CustomerDTO dto : dtos) {
			customers.add(ProxyConverter.convert2Proxy(dto));
		}

		return customers;
	}


}
