package com.gp.training.web.server.utils;

import com.gp.training.bizlogic.api.model.*;
import com.gp.training.web.shared.model.*;

/**
 * Created by tondi on 2/6/16.
 */
public class ProxyConverter {
	private ProxyConverter() {
	}

	public static OfferProxy convert2Proxy(OfferDTO offerDTO) {
		OfferProxy proxy = new OfferProxy();
		if (offerDTO != null) {
			proxy.setOfferId(offerDTO.getOfferId().intValue());

			HotelDTO hotelDTO = offerDTO.getHotel();
			HotelProxy hotelProxy = convert2Proxy(hotelDTO);

			proxy.setHotel(hotelProxy);

			RoomTypeProxy roomProxy = convert2Proxy(offerDTO.getRoom());
			proxy.setRoom(roomProxy);
			if(offerDTO.getPrice() != null) {
				proxy.setPrice(offerDTO.getPrice().doubleValue());
			}
		}
		return proxy;
	}

	public static RoomTypeProxy convert2Proxy(RoomTypeDTO roomDTO) {
		RoomTypeProxy roomProxy = new RoomTypeProxy();
		if (roomDTO != null) {
			roomProxy.setCode(roomDTO.getCode());
			roomProxy.setName(roomDTO.getName());
			if (roomDTO.getId() != null) {
				roomProxy.setId(roomDTO.getId().intValue());
			}
		}
		return roomProxy;
	}

	public static HotelProxy convert2Proxy(HotelDTO hotelDTO) {
		HotelProxy hotelProxy = new HotelProxy();
		if (hotelDTO != null) {
			hotelProxy.setCode(hotelDTO.getCode());
			hotelProxy.setName(hotelDTO.getName());
			if (hotelDTO.getId() != null) {
				hotelProxy.setId(hotelDTO.getId().intValue());
			}
			if (hotelDTO.getCity() != null) {
				hotelProxy.setCity(convert2Proxy(hotelDTO.getCity()));
			}
		}
		return hotelProxy;
	}


	public static CountryProxy convert2Proxy(CountryDTO dto) {
		CountryProxy proxy = new CountryProxy();
		if (dto != null) {
			if (dto.getId() != null) {
				proxy.setId(dto.getId().intValue());
			}
			proxy.setName(dto.getName());
			proxy.setCode(dto.getCode());
		}
		return proxy;
	}

	public static MealTypeProxy convert2Proxy(MealTypeDTO dto) {
		MealTypeProxy proxy = new MealTypeProxy();
		if (dto != null) {
			proxy.setId(dto.getId());
			proxy.setName(dto.getName());
			proxy.setCode(dto.getCode());
		}
		return proxy;
	}

	public static CityProxy convert2Proxy(CityDTO dto) {
		CityProxy proxy = new CityProxy();
		if (dto != null) {
			if (dto.getId() != null) {
				proxy.setId(dto.getId().intValue());
			}
			proxy.setCode(dto.getCode());
			proxy.setName(dto.getName());

			proxy.setCountry(convert2Proxy(dto.getCountry()));
		}
		return proxy;
	}

	public static CustomerProxy convert2Proxy(CustomerDTO dto) {
		CustomerProxy p = new CustomerProxy();
		if (dto != null) {
			if (dto.getId() != null) {
				p.setId(dto.getId().intValue());
			}
			p.setFirstName(dto.getFirstName());
			p.setLastName(dto.getLastName());
			p.setEmail(dto.getEmail());
			p.setPhone(dto.getPhoneNumber());
		}
		return p;
	}

	public static BookingProxy convert2Proxy(BookingDTO dto) {
		BookingProxy proxy = new BookingProxy();
		if (dto != null) {
			proxy.setId((int) dto.getId());
			proxy.setGuestCount(dto.getGuestCount());
			proxy.setMealType(convert2Proxy(dto.getMealType()));
			proxy.setOffer(convert2Proxy(dto.getOffer()));
			if (dto.getStartDate() != null) {
				proxy.setStartDate(dto.getStartDate());
			}
			if (dto.getEndDate() != null) {
				proxy.setEndDate(dto.getEndDate());
			}
			if (dto.getCustomers() != null) {
				for (CustomerDTO customer : dto.getCustomers()) {
					proxy.getCustomers().add(convert2Proxy(customer));
				}
			}
		}

		return proxy;
	}
}
