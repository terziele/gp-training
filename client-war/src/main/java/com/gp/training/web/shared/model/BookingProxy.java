package com.gp.training.web.shared.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jboss.errai.common.client.api.annotations.MapsTo;
import org.jboss.errai.common.client.api.annotations.Portable;

@Portable
public class BookingProxy {
	private int id;
	private OfferProxy offer;
	private MealTypeProxy mealType;
	private Date startDate;
	private Date endDate;
	private int guestCount;
	private List<CustomerProxy> customers = new ArrayList<>();

	public BookingProxy() {}

	public BookingProxy(@MapsTo("id") int id,@MapsTo("offer") OfferProxy offer,@MapsTo("mealType") MealTypeProxy mealType,
	                    @MapsTo("startDate") Date startDate,@MapsTo("endDate") Date endDate,@MapsTo("guestCount") int guestCount,
	                    @MapsTo("customers") List<CustomerProxy> customers) {
		this.id = id;
		this.offer = offer;
		this.mealType = mealType;
		this.startDate = startDate;
		this.endDate = endDate;
		this.guestCount = guestCount;
		this.customers = customers;
	}

	public List<CustomerProxy> getCustomers() {
		return customers;
	}

	public void setCustomers(List<CustomerProxy> customers) {
		this.customers = customers;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getGuestCount() {
		return guestCount;
	}

	public void setGuestCount(int guestCount) {
		this.guestCount = guestCount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public OfferProxy getOffer() {
		return offer;
	}

	public void setOffer(OfferProxy offer) {
		this.offer = offer;
	}

	public MealTypeProxy getMealType() {
		return mealType;
	}

	public void setMealType(MealTypeProxy mealType) {
		this.mealType = mealType;
	}
}
