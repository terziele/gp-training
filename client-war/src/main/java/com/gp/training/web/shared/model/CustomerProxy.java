package com.gp.training.web.shared.model;

import org.jboss.errai.common.client.api.annotations.MapsTo;
import org.jboss.errai.common.client.api.annotations.Portable;

/**
 * Created by tondi on 2/2/16.
 */
@Portable
public class CustomerProxy {
	private int id;
	private String firstName;
	private String lastName;
	private String email;
	private String phone;
	private int bookingId;

	public CustomerProxy() {
	}

	public CustomerProxy(@MapsTo("id") int id, @MapsTo("firstName") String firstName, @MapsTo("lastName") String lastName,
	                     @MapsTo("email") String email, @MapsTo("phone") String phone,
	                     @MapsTo("bookingId") int bookingId) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phone = phone;
		this.bookingId = bookingId;
	}

	public int getBookingId() {
		return bookingId;
	}

	public void setBookingId(int bookingId) {
		this.bookingId = bookingId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
}
