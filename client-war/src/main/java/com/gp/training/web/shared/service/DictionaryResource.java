package com.gp.training.web.shared.service;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.gp.training.web.shared.model.CityProxy;
import com.gp.training.web.shared.model.CountryProxy;
import com.gp.training.web.shared.model.MealTypeProxy;


@Path("/dictionary")
public interface DictionaryResource {
	
	@GET
	@Path("country")
	@Produces(MediaType.APPLICATION_JSON)
	public List<CountryProxy> getCountries();
	
	@GET
	@Path("city")
	@Produces(MediaType.APPLICATION_JSON)
	public List<CityProxy> getCities();

	@GET
	@Path("/country/cities")
	@Produces(MediaType.APPLICATION_JSON)
	List<CityProxy> getCitiesByCountry(@QueryParam("country") Long id);

	@GET
	@Path("/meals")
	@Produces(MediaType.APPLICATION_JSON)
	public List<MealTypeProxy> getMealTypes();
}
