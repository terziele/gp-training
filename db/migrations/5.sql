-- Таблица отелей
CREATE TABLE hotel (
  id      INT(11) PRIMARY KEY  AUTO_INCREMENT,
  city_id INT(11)     NOT NULL,
  code    VARCHAR(10) NOT NULL,
  name    VARCHAR(64) NOT NULL,
  FOREIGN KEY (city_id) REFERENCES city (id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

INSERT INTO hotel (city_id, code, name)
VALUES (1, "BHM", "Belarus Hotel"), (1, "VHM", "Victoria Hotel"), (1, "PHM", "President Hotel"),
  (2, "SHG", "Semashko Hotel"), (2, "NG", "Neman"),
  (3, "HDLP", "Hotel Du Louvre"), (3, "NPP", "Napoleon Paris"),
  (4, "BRN", "Beau Rivage"), (4, "HNN", "Hotel Negresco");

-- Таблица предложений
CREATE TABLE offer (
  id           INT(11) PRIMARY KEY  AUTO_INCREMENT,
  hotel_id     INT(11) NOT NULL,
  room_type_id INT(11) NOT NULL,
  price        BIGINT  NOT NULL,
  FOREIGN KEY (hotel_id) REFERENCES hotel (id),
  FOREIGN KEY (room_type_id) REFERENCES room_type (id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
