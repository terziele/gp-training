-- Таблица букингов
create table booking (
	id int(11) primary key auto_increment,
	offer_id int(11) not null,
	meal_type_id int(11) not null,
	start_date DATE not null,
	end_date DATE not null,
	guest_count int(2) not null,
	foreign key (meal_type_id) references meal_type(id),
	foreign key (offer_id) references offer(id)
) engine=InnoDB default charset=utf8;

drop table booking;