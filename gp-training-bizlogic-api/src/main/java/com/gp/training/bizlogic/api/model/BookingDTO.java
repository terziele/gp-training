package com.gp.training.bizlogic.api.model;

import javax.xml.bind.annotation.*;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "booking")
@XmlAccessorType(XmlAccessType.FIELD)
public class BookingDTO {

	@XmlAttribute(name = "id")
	private long id;
	@XmlElement(name = "offer")
	private OfferDTO offer;
	@XmlElement(name = "meal_type")
	private MealTypeDTO mealType;
	@XmlElement(name = "guest_count")
	private int guestCount;
	@XmlElement(name = "start_date")
	private Date startDate;
	@XmlElement(name = "end_date")
	private Date endDate;
	@XmlElement(name = "customers")
	private List<CustomerDTO> customers = new ArrayList<>();

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getGuestCount() {
		return guestCount;
	}

	public void setGuestCount(int guestCount) {
		this.guestCount = guestCount;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public OfferDTO getOffer() {
		return offer;
	}

	public void setOffer(OfferDTO offer) {
		this.offer = offer;
	}

	public MealTypeDTO getMealType() {
		return mealType;
	}

	public void setMealType(MealTypeDTO mealType) {

		this.mealType = mealType;
	}

	public List<CustomerDTO> getCustomers() {
		return customers;
	}

	public void setCustomers(List<CustomerDTO> customers) {
		this.customers = customers;
	}
}
