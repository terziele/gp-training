package com.gp.training.bizlogic.api.params;


import com.gp.training.bizlogic.api.model.MealTypeDTO;

import javax.xml.bind.annotation.*;
import java.util.Date;

@XmlRootElement(name = "bookingCreateParams")
@XmlAccessorType(XmlAccessType.FIELD)
public class BookingCreateParams {

	@XmlElement(name = "offer_id")
	private long offerId;
	@XmlElement(name = "guest_count")
	private int guestCount;
	@XmlElement(name = "start_date")
	private Date startDate;
	@XmlElement(name = "end_date")
	private Date endDate;
	@XmlElement(name = "meal_type")
	private int  mealTypeId;


	public static class Builder {
		private long offerId;
		private int guestCount;
		private Date startDate;
		private Date endDate;
		private int mealTypeId;

		public Builder offerId(long offerId) {
			this.offerId = offerId;
			return this;
		}

		public Builder guestCount(int guestCount) {
			this.guestCount = guestCount;
			return this;
		}

		public Builder startDate(Date startDate) {
			this.startDate = startDate;
			return this;
		}

		public Builder endDate(Date endDate) {
			this.endDate = endDate;
			return this;
		}

		public Builder mealType(int mealTypeId) {
			this.mealTypeId = mealTypeId;
			return this;
		}

		public BookingCreateParams build() {
			BookingCreateParams params = new BookingCreateParams();
			params.offerId = offerId;
			params.guestCount = guestCount;
			params.startDate = startDate;
			params.endDate = endDate;
			params.mealTypeId = mealTypeId;
			return params;
		}


	}

	public long getMealTypeId() {
		return mealTypeId;
	}
	public long getOfferId() {
		return offerId;
	}

	public int getGuestCount() {
		return guestCount;
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getEndDate() {
		return endDate;
	}
}
