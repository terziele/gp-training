package com.gp.training.bizlogic.api.resource;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.gp.training.bizlogic.api.model.BookingDTO;
import com.gp.training.bizlogic.api.params.BookingCreateParams;

@Path("/booking")
public interface BookingResource {
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BookingDTO create(BookingCreateParams params);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	BookingDTO get(@PathParam("id") long id);
}
