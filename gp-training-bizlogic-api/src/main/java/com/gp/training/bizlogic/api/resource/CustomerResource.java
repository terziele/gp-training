package com.gp.training.bizlogic.api.resource;

import com.gp.training.bizlogic.api.model.BookingDTO;
import com.gp.training.bizlogic.api.model.CustomerDTO;
import com.gp.training.bizlogic.api.params.BookingCreateParams;
import com.gp.training.bizlogic.api.params.CustomerCreateParams;
import javafx.scene.media.Media;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/customer")
public interface CustomerResource {

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    List<CustomerDTO> create(List<CustomerCreateParams> params);

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public CustomerDTO get(@PathParam("id") long id);


}
