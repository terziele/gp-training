package com.gp.training.bizlogic.api.resource;

import com.gp.training.bizlogic.api.model.MealTypeDTO;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/mealType")
public interface MealTypeResource {

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public MealTypeDTO get(@PathParam("id") long id);

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<MealTypeDTO> getAll();
}
