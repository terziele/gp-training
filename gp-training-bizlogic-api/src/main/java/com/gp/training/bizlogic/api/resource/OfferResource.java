package com.gp.training.bizlogic.api.resource;

import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.gp.training.bizlogic.api.model.OfferDTO;


@Path("/availability")
public interface OfferResource {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<OfferDTO> findOffers(@QueryParam("cityId") long cityId,
			@QueryParam("guestCount") int guestCount,
			@QueryParam("startDate") String startDate,
			@QueryParam("endDate") String endDate) ;

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	OfferDTO get(@PathParam("id") long id);
}
