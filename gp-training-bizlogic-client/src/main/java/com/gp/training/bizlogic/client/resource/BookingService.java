package com.gp.training.bizlogic.client.resource;

import com.gp.training.bizlogic.api.model.BookingDTO;
import com.gp.training.bizlogic.api.params.BookingCreateParams;

public interface BookingService {
	
	public BookingDTO createBooking(BookingDTO booking);

	BookingDTO create(BookingCreateParams params);

	BookingDTO get(long id);
}
