package com.gp.training.bizlogic.client.resource;

import com.gp.training.bizlogic.api.params.BookingCreateParams;
import com.gp.training.bizlogic.api.resource.BookingResource;
import com.gp.training.bizlogic.client.ClientSender;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gp.training.bizlogic.api.model.BookingDTO;

@Component
public class BookingServiceImpl implements BookingService {
	@Autowired
	private ClientSender sender;

	@Override
	public BookingDTO createBooking(BookingDTO booking) {
		ResteasyWebTarget webTarget = sender.getRestEasyTarget();
		BookingResource resource = webTarget.proxy(BookingResource.class);
		BookingCreateParams params = new BookingCreateParams.Builder()
				.offerId(booking.getOffer().getOfferId())
				.guestCount(booking.getGuestCount())
				.startDate(booking.getStartDate())
				.endDate(booking.getEndDate())
				.mealType(booking.getMealType().getId())
				.build();
		BookingDTO dto = resource.create(params);

		return dto;
	}

	@Override
	public BookingDTO create(BookingCreateParams params) {
		ResteasyWebTarget webTarget = sender.getRestEasyTarget();
		BookingResource resource = webTarget.proxy(BookingResource.class);
		BookingDTO dto = resource.create(params);
		webTarget.getResteasyClient().close();
		return dto;
	}

	@Override
	public BookingDTO get(long id) {
		ResteasyWebTarget wt = sender.getRestEasyTarget();
		BookingResource rsc = wt.proxy(BookingResource.class);
		BookingDTO dto = rsc.get(id);
		wt.getResteasyClient().close();
		return dto;
	}
}
