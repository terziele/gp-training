package com.gp.training.bizlogic.client.resource;

import com.gp.training.bizlogic.api.model.CustomerDTO;
import com.gp.training.bizlogic.api.params.CustomerCreateParams;
import com.gp.training.bizlogic.api.resource.CustomerResource;
import com.gp.training.bizlogic.client.ClientSender;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private ClientSender sender;

	@Override
	public CustomerDTO get(long id) {
		ResteasyWebTarget webTarget = sender.getRestEasyTarget();
		CustomerResource resource = webTarget.proxy(CustomerResource.class);
		CustomerDTO dto = resource.get(id);
		webTarget.getResteasyClient().close();
		return dto;

	}

	@Override
	public CustomerDTO save(CustomerDTO dto) {
		ResteasyWebTarget webTarget = sender.getRestEasyTarget();
		CustomerResource resource = webTarget.proxy(CustomerResource.class);
//		dto = resource.create(dto);
		webTarget.getResteasyClient().close();
		return dto;
	}

	@Override
	public List<CustomerDTO> create(List<CustomerCreateParams> params) {
		ResteasyWebTarget wt = sender.getRestEasyTarget();
		CustomerResource rsc = wt.proxy(CustomerResource.class);
		List<CustomerDTO> dtos = rsc.create(params);
		wt.getResteasyClient().close();
		return dtos;

	}
}
