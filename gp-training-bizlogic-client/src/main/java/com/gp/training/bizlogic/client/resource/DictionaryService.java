package com.gp.training.bizlogic.client.resource;

import java.util.List;

import com.gp.training.bizlogic.api.model.CityDTO;
import com.gp.training.bizlogic.api.model.CountryDTO;
import com.gp.training.bizlogic.api.model.MealTypeDTO;

public interface DictionaryService {
	
	public List<CountryDTO> getCountries();
	
	public List<CityDTO> getCities();

	List<CityDTO> getCitiesByCountry(long id);
	List<MealTypeDTO> getMeals();
}
