package com.gp.training.bizlogic.client.resource;

import java.util.List;

import com.gp.training.bizlogic.api.model.OfferDTO;

public interface OfferService {
	
	public List<OfferDTO> getOffers(long cityId, int guestCount, String startDate, String endDate);

	OfferDTO get(long offerId);
}
