package com.gp.training.bizlogic.client.resource;

import java.util.List;

import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gp.training.bizlogic.api.model.OfferDTO;
import com.gp.training.bizlogic.api.resource.OfferResource;
import com.gp.training.bizlogic.client.ClientSender;

@Component
public class OfferServiceImpl implements OfferService {
	
	@Autowired
	private ClientSender sender;
	
	@Override
	public List<OfferDTO> getOffers(long cityId, int guestCount, String startDate, String endDate) {
		
		ResteasyWebTarget webTarget = sender.getRestEasyTarget();
		OfferResource offerProxy = webTarget.proxy(OfferResource.class);
		
		List<OfferDTO> offers = offerProxy.findOffers(cityId, guestCount, startDate, endDate);
		webTarget.getResteasyClient().close();
		return offers;
	}

	@Override
	public OfferDTO get(long id) {
		ResteasyWebTarget webTarget = sender.getRestEasyTarget();
		OfferResource proxy = webTarget.proxy(OfferResource.class);

		OfferDTO offer = proxy.get(id);
		webTarget.getResteasyClient().close();
		return offer;
	}

}
