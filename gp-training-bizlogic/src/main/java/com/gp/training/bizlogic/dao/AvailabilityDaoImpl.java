package com.gp.training.bizlogic.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gp.training.bizlogic.domain.Offer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.gp.training.bizlogic.api.model.HotelDTO;
import com.gp.training.bizlogic.api.model.OfferDTO;
import com.gp.training.bizlogic.api.model.RoomTypeDTO;
import com.gp.training.bizlogic.params.AvailSearchParams;
import com.gp.training.bizlogic.utils.DateUtils;

@Repository
public class AvailabilityDaoImpl implements AvailabilityDao {
	
	private static final String SELECT_SUITABLE_OFFERS_QUERY =
			"select o.id as offerId, rt.name as roomTypeName, h.name as hotelName, o.price  as price  \n"
			+ "from offer o left join hotel h on o.hotel_id=h.id \n" 
			+			  " left join room_type rt on o.room_type_id = rt.id \n"
			+             " left join offer_availability oa on o.id=oa.offer_id \n"
			+ "where h.city_id = :cityId and rt.guest_count = :guestCount ";

	private static final String SELECT_OFFER = "select o.id as offerId, rt.name as roomTypeName, h.name as hotelName " +
			" from offer o  join hotel h on o.hotel_id = h.id\n" +
			"join room_type rt on o.room_type_id = rt.id where o.id = :id";

	private OfferMapper mapper = new OfferMapper();
	@Autowired
	private NamedParameterJdbcTemplate namedJDBCTemplate;

	@Override
	public List<OfferDTO> findOffers(AvailSearchParams params) {
		
		String dayPredicate = buildDayPredicate(params.getStartDate(), params.getEndDate());
		String fullPredicate = SELECT_SUITABLE_OFFERS_QUERY + " and " + dayPredicate;
		
		Map<String, Object> paramMap = parseParams(params);
		
		List<OfferDTO> offers = namedJDBCTemplate.query(fullPredicate, paramMap, mapper);
		
		return offers;
	}

	@Override
	public OfferDTO get(long id) {
		Map<String, Long> params = new HashMap<>();
		params.put("id", id);
		OfferDTO dto = (OfferDTO) namedJDBCTemplate.query(SELECT_OFFER, params, mapper).get(0);
		return dto;
	}


	private Map<String, Object> parseParams(AvailSearchParams params) {
		Map<String, Object> paramMap = new HashMap<>();
		
		paramMap.put("cityId", params.getCityId());
		paramMap.put("guestCount", params.getGuestCount());
		
		return paramMap;
	}
	
	private String buildDayPredicate(Date startDate, Date endDate) {
		int startDayNumber = DateUtils.getDifferenceFromBegining(startDate);
		int endDayNumber = DateUtils.getDifferenceFromBegining(endDate);
		
		return buildDayPredicate(startDayNumber, endDayNumber);
	}
	
	private String buildDayPredicate(int startDayNumber, int endDayNumber) {
		StringBuilder builder = new StringBuilder();
		
		if (endDayNumber > startDayNumber) {
			
			for (int i=startDayNumber; i < endDayNumber; i++){
				if(builder.length() > 0) {
					builder.append(" and ");
				}
				
				builder.append("`" + i + "` > 0");
			}
		}
		
		return builder.toString();
	}

	private class OfferMapper implements RowMapper {

		@Override
		public OfferDTO mapRow(ResultSet rs, int arg1) throws SQLException {
			OfferDTO dto = new OfferDTO();
			dto.setOfferId(rs.getLong("offerId"));
			dto.setPrice(rs.getBigDecimal("price"));

			HotelDTO hotelDTO = new HotelDTO();
			hotelDTO.setName(rs.getString("hotelName"));
			dto.setHotel(hotelDTO);

			RoomTypeDTO roomDTO = new RoomTypeDTO();
			roomDTO.setName(rs.getString("roomTypeName"));
			dto.setRoom(roomDTO);

			return dto;
		}
	}
}
