package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.api.params.BookingCreateParams;
import com.gp.training.bizlogic.domain.Booking;
import com.gp.training.bizlogic.domain.MealType;

public interface BookingDao {
	
	public Booking create(BookingCreateParams booking);

	Booking get(long id);
}
