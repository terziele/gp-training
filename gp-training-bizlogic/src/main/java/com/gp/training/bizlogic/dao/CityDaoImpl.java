package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.domain.Country;
import org.springframework.stereotype.Repository;

import com.gp.training.bizlogic.domain.City;

import java.util.List;

@Repository
public class CityDaoImpl extends GenericDaoImpl<City, Long> implements CityDao {

	public CityDaoImpl(){
		super(City.class);
	}

	@Override
	public List<City> getByCountry(long id) {
		return em.createNamedQuery(City.GET_BY_COUNTRY_ID, City.class)
				.setParameter("id", id)
				.getResultList();
	}
}
