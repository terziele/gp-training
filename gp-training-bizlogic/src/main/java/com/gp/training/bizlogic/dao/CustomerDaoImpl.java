package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.api.params.CustomerCreateParams;
import com.gp.training.bizlogic.domain.Booking;
import com.gp.training.bizlogic.domain.Customer;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerDaoImpl extends GenericDaoImpl<Customer, Long> implements CustomerDao {
	public CustomerDaoImpl() {
		super(Customer.class);
	}

	@Override
	@Transactional(readOnly = false)
	public Customer save(Customer customer) {
		return em.merge(customer);
	}

	@Override
	@Transactional(readOnly = false)
	public List<Customer> save(List<CustomerCreateParams> params) {
		List<Customer> customers = new ArrayList<>(params.size());
		for(CustomerCreateParams param : params) {
//			customers.add(convertParams(param));
			customers.add(em.merge(convertParams(param)));
		}

		return customers;
	}

	private Customer convertParams(CustomerCreateParams param) {
		Customer c = new Customer();
		c.setFirstName(param.getFirstName());
		c.setLastName(param.getLastName());
		c.setEmail(param.getEmail());
		c.setPhoneNumber(param.getPhone());
		Booking b = em.find(Booking.class, param.getBookingId());
		b.getCustomers().add(c);
		c.getBookings().add(b);
		return c;
	}
}
