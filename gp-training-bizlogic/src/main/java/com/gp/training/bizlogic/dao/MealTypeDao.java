package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.domain.MealType;

/**
 * Created by tondi on 2/3/16.
 */
public interface MealTypeDao extends GenericDao<MealType, Long> {
}
