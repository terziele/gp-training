package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.domain.MealType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class MealTypeDaoImpl extends GenericDaoImpl<MealType, Long> implements MealTypeDao {

	private static final String GET_MEAL_TYPE = "select * from meal_type m where m.id = :id";
	public MealTypeDaoImpl() {
		super(MealType.class);
	}

	@Autowired
	private JdbcTemplate template;

}
