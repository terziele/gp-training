package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.domain.Offer;

/**
 * Created by tondi on 2/3/16.
 */
public interface OfferDao extends GenericDao<Offer, Long> {
}
