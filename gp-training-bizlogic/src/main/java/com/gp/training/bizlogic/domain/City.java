package com.gp.training.bizlogic.domain;

import javax.persistence.*;

@Entity
@Table(name="city")
@NamedQueries({
		@NamedQuery(name = City.GET_BY_COUNTRY_ID, query = "select c from City c where c.country.id = :id")
})
public class City extends AbstractEntity {
	public static final String GET_BY_COUNTRY_ID = "getCitiesByCountryId";
	
	@Column(name="name", insertable=true, nullable=false, unique=true, updatable=true)
	private String name;
	
	@Column(name="code", insertable=true, nullable=false, unique=true, updatable=true)
	private String code;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="country_id")
	private Country country;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}
}
