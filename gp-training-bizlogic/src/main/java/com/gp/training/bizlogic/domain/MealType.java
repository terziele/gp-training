package com.gp.training.bizlogic.domain;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "meal_type")
@NamedQueries({
		@NamedQuery(name = MealType.FIND_BY_ID, query = "Select m from MealType m where m.id = :id")
})
public class MealType extends AbstractEntity {

	public static final String FIND_BY_ID = "findMealById";
	@Column(name = "code")
	private String code;
	@Column(name = "name")
	private String name;

	@OneToMany(fetch=FetchType.LAZY)
//	@JoinColumn(name = "booking_id")
	private List<Booking> booking;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		MealType mealType = (MealType) o;

		if (code != null ? !code.equals(mealType.code) : mealType.code != null) return false;
		return name != null ? name.equals(mealType.name) : mealType.name == null;

	}

	@Override
	public int hashCode() {
		int result = code != null ? code.hashCode() : 0;
		result = 31 * result + (name != null ? name.hashCode() : 0);
		return result;
	}
}
