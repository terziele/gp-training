package com.gp.training.bizlogic.domain;

import com.gp.training.bizlogic.api.model.RoomTypeDTO;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "offer")
public class Offer extends AbstractEntity {

	@Column(name = "price")
	private BigDecimal price;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "hotel_id")
	private Hotel hotel;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "room_type_id")
	private RoomType roomType;

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	public RoomType getRoomType() {
		return roomType;
	}

	public void setRoomType(RoomType roomType) {
		this.roomType = roomType;
	}
}
