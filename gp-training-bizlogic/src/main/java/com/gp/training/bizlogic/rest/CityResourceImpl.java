package com.gp.training.bizlogic.rest;

import java.util.ArrayList;
import java.util.List;

import com.gp.training.bizlogic.utils.DtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gp.training.bizlogic.api.model.CityDTO;
import com.gp.training.bizlogic.api.resource.CityResource;
import com.gp.training.bizlogic.dao.CityDao;
import com.gp.training.bizlogic.domain.City;

@Component
public class CityResourceImpl implements CityResource {
	
	@Autowired
	private CityDao cityDao;
	
	@Override
	public CityDTO getCity(long cityId) {
		City city = cityDao.get(Long.valueOf(cityId));
		return DtoConverter.convert2Dto(city);
	}
	
	@Override
	public List<CityDTO> getCities() {
		List<City> cities = cityDao.getAll();
		
		List<CityDTO> cityDtos = new ArrayList<>(cities.size());
		for (City city : cities) {
			CityDTO dto = DtoConverter.convert2Dto(city);
			cityDtos.add(dto);
		}
		
		return cityDtos;
		
		/*return cities.stream().map(c -> convert2Dto(c)).collect(Collectors.toList());*/
	}

	@Override
	public List<CityDTO> getCitiesByCountry(long id) {
		List<City> cities = cityDao.getByCountry(id);

		List<CityDTO> cityDtos = new ArrayList<>(cities.size());
		for (City city : cities) {
			CityDTO dto = DtoConverter.convert2Dto(city);
			cityDtos.add(dto);
		}

		return cityDtos;
	}


}
