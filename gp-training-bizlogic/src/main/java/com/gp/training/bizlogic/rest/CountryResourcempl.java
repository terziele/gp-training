package com.gp.training.bizlogic.rest;

import java.util.ArrayList;
import java.util.List;

import com.gp.training.bizlogic.utils.DtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gp.training.bizlogic.api.model.CountryDTO;
import com.gp.training.bizlogic.api.resource.CountryResource;
import com.gp.training.bizlogic.dao.CountryDao;
import com.gp.training.bizlogic.domain.Country;

@Component
public class CountryResourcempl implements CountryResource {
	
	@Autowired
	private CountryDao countryDao;
	
	@Override
	public CountryDTO getCountry(long countryId) {
		Country country = countryDao.get(Long.valueOf(countryId));	
		return DtoConverter.convert2Dto(country);
	}
	
	@Override
	public List<CountryDTO> getCountries() {
		List<Country> countries = countryDao.getAll();
		
		List<CountryDTO> countryDtos = new ArrayList<>(countries.size());
		for (Country country : countries) {
			CountryDTO dto = DtoConverter.convert2Dto(country);
			countryDtos.add(dto);
		}
		
		return countryDtos;	
		/*return countries.stream().map(c -> convert2Dto(c)).collect(Collectors.toList());*/
	}
		

}
