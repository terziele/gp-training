package com.gp.training.bizlogic.rest;

import com.gp.training.bizlogic.api.model.CustomerDTO;
import com.gp.training.bizlogic.api.params.CustomerCreateParams;
import com.gp.training.bizlogic.api.resource.CustomerResource;
import com.gp.training.bizlogic.dao.CustomerDao;
import com.gp.training.bizlogic.domain.Customer;
import com.gp.training.bizlogic.utils.DtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CustomerResourceImpl implements CustomerResource {

	@Autowired
	private CustomerDao customerDao;

	@Override
	public List<CustomerDTO> create(List<CustomerCreateParams> params) {
		List<Customer> customers = customerDao.save(params);
		List<CustomerDTO> dtos = new ArrayList<>(customers.size());
		for(Customer customer : customers) {
			dtos.add(DtoConverter.convert2Dto(customer));
		}
		return dtos;
	}

		@Override
	public CustomerDTO get(long id) {
		Customer customer = customerDao.get(id);

		return DtoConverter.convert2Dto(customer);
	}

}
