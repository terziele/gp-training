package com.gp.training.bizlogic.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gp.training.bizlogic.api.model.HotelInfoDTO;
import com.gp.training.bizlogic.api.resource.HotelInfoResource;
import com.gp.training.bizlogic.dao.HotelInfoDao;

@Component
public class HotelInfoResourceImpl implements HotelInfoResource {
	
	@Autowired
	HotelInfoDao hotelInfoDao;
	
	@Override
	public HotelInfoDTO getHotelInfo(long hotelId) {

		HotelInfoDTO hotelInfo = hotelInfoDao.getHotelInfo(hotelId);
		return hotelInfo;
	}
}
