package com.gp.training.bizlogic.rest;

import com.gp.training.bizlogic.api.model.MealTypeDTO;
import com.gp.training.bizlogic.api.resource.MealTypeResource;
import com.gp.training.bizlogic.dao.MealTypeDao;
import com.gp.training.bizlogic.domain.MealType;
import com.gp.training.bizlogic.utils.DtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MealTypeResourceImpl implements MealTypeResource {

	@Autowired
	private MealTypeDao mealTypeDao;

	@Override
	public MealTypeDTO get(long id) {

		return DtoConverter.convert2Dto(mealTypeDao.get(id));
	}

	@Override
	public List<MealTypeDTO> getAll() {
		List<MealType> meals = mealTypeDao.getAll();
		List<MealTypeDTO> dtos = new ArrayList<>(meals.size());
		for(MealType meal : meals) {
			dtos.add(DtoConverter.convert2Dto(meal));
		}
		return dtos;
	}


}
