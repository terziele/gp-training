package com.gp.training.bizlogic.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {
	
	private static Date BEGIN_DATE = calculateBeginDate();
	private static Date calculateBeginDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(2000, Calendar.JANUARY, 1, 0, 0, 0);
		return calendar.getTime();
	}
	
	private static final ThreadLocal<SimpleDateFormat> ISO_DATE_FORMAT = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyyy-MM-dd");
		}
	};
	
	public static Date convertISODateStringToDate(final String dateStr) {
		Date date = null;

		try {
			date = ISO_DATE_FORMAT.get().parse(dateStr);
		} catch (final ParseException e) {	
		}
		return date;
	} 

	public static String formatDateToISO(final Date date) {
		return ISO_DATE_FORMAT.get().format(date);
	}
	
	public static Integer getDifferenceFromBegining(Date date) {
		return getDifferenceInDaysAccurate(BEGIN_DATE, date);
	}
	
	public static Integer getDifferenceInDaysAccurate(final Date d1, final Date d2) {
        final int diffInMinutes = (int) (d2.getTime() / (1000 * 60 * 60 * 24) - d1.getTime() / (1000 * 60 * 60 * 24));
        return Integer.valueOf(diffInMinutes);
    }
}
