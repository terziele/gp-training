package com.gp.training.bizlogic.utils;

import com.gp.training.bizlogic.api.model.*;
import com.gp.training.bizlogic.domain.*;

import java.util.Date;

/**
 * Created by tondi on 2/6/16.
 */
public class DtoConverter {
	private DtoConverter() {}

	public static CityDTO convert2Dto(City city) {
		CityDTO dto = new CityDTO();
		if (city != null) {
			dto.setId(city.getId());
			dto.setName(city.getName());
			dto.setCode(city.getCode());

			if (city.getCountry() != null) {
				dto.setCountry(convert2Dto(city.getCountry()));
			}
		}
		return dto;
	}

	public static CountryDTO convert2Dto(Country country) {
		CountryDTO dto = new CountryDTO();
		if (country != null) {
			dto.setId(country.getId());
			dto.setName(country.getName());
			dto.setCode(country.getCode());
		}
		return dto;
	}

	public static CustomerDTO convert2Dto(Customer item) {
		CustomerDTO dto = new CustomerDTO();
		if (item != null) {
			dto.setId(item.getId());
			dto.setFirstName(item.getFirstName());
			dto.setLastName(item.getLastName());
			dto.setEmail(item.getEmail());
			dto.setPhoneNumber(item.getPhoneNumber());
		}
		return dto;
	}

	public static MealTypeDTO convert2Dto(MealType m) {
		MealTypeDTO dto = new MealTypeDTO();
		if(m != null) {
			dto.setId(m.getId().intValue());
			dto.setCode(m.getCode());
			dto.setName(m.getName());
		}
		return dto;
	}

	public static BookingDTO convert2Dto(Booking entity) {
		BookingDTO dto = new BookingDTO();
		dto.setId(entity.getId());

		if(entity.getOffer() != null) {
			OfferDTO offer = new OfferDTO();
			offer.setPrice(entity.getOffer().getPrice());
			offer.setHotel(convert2Dto(entity.getOffer().getHotel()));
			offer.setRoom(convert2Dto(entity.getOffer().getRoomType()));
			offer.setOfferId(entity.getOffer().getId());
			dto.setOffer(offer);
		}
		if(entity.getMealType() != null) {
			MealTypeDTO mealDto = new MealTypeDTO();
			MealType meal = entity.getMealType();
			mealDto.setId(meal.getId().intValue());
			mealDto.setName(meal.getName());
			mealDto.setCode(meal.getCode());
			dto.setMealType(mealDto);
		}
		if(entity.getCustomers() != null) {
			for(Customer customer : entity.getCustomers()) {
				dto.getCustomers().add(convert2Dto(customer));
			}
		}
		if (entity.getEndDate() != null) {
			dto.setEndDate(entity.getEndDate());
		}
		if (entity.getStartDate() != null) {
			dto.setStartDate(entity.getStartDate());
		}
		dto.setGuestCount(entity.getGuestCount());
		return dto;
	}

	private static RoomTypeDTO convert2Dto(RoomType room) {
		RoomTypeDTO dto = new RoomTypeDTO();
		if(room != null) {
			dto.setId(room.getId());
			dto.setName(room.getName());
			dto.setCode(room.getCode());
		}
		return dto;
	}

	public static HotelDTO convert2Dto(Hotel hotel) {
		HotelDTO dto = new HotelDTO();
		if(hotel != null) {
			if(hotel.getId() != null) {
				dto.setId(hotel.getId());
			}
			if(hotel.getCity() != null) {
				dto.setCity(convert2Dto(hotel.getCity()));
			}
			dto.setName(hotel.getName());
			dto.setCode(hotel.getCode());
		}
		return dto;
	}
}
